#--------------compute/main.tf--------


#-------aws-ami-search--------
data "aws_ami" "terraform_ami_search" {
    owners = ["amazon"]
    most_recent = true
    
    filter {
        name = "name"
        values = ["amzn-ami-hvm*-x86_64-gp2"]
    }
}


#----public key----
resource "aws_key_pair" "ec2_key_pair" {
    key_name = "${var.key_name}"
    public_key = "${file(var.key_path)}"
}

data "template_file" "user-init" {
    count    = 2
    template = "${file("${path.module}/userdata.tpl")}"
    
    vars = {
        firewall_subnets = "${element(var.pub_cidrs, count.index)}"
    }
}

#---EC2 instance----
resource "aws_instance" "terraform_web_server" {
    count = "${var.instance_count}"
    instance_type = "${var.instance_type}"
    ami = "${data.aws_ami.terraform_ami_search.id}"
    
    key_name = "${aws_key_pair.ec2_key_pair.key_name}"
    
    vpc_security_group_ids = ["${var.security_group}"]
    subnet_id = "${element(var.public_subnet_id , count.index)}"
    associate_public_ip_address = true
    user_data = "${data.template_file.user-init.*.rendered[count.index]}"
    
    tags = {
        name = "tf_web_server${count.index + 1}"
    }
}