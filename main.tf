#----main.tf-----

provider "aws" {
    region = "${var.aws_region}"
}

#------ deploy IAM user----

module "terraform_iam_user" {
    source = "./iam-user"
    create_user = "${var.create_user}"
    create_iam_user_login_profile = "${var.create_iam_user_login_profile}"
    iam_name = "${var.iam_name}"
    path = "${var.path}"
    force_destroy = "${var.force_destroy}"
    pgp_key = "${var.pgp_key}"
    password_reset_required = "${var.password_reset_required}"
    password_length = "${var.password_length}"
    permissions_boundary = "${var.permissions_boundary}"
}

#------ deploy iam-group-with-policies----

module "terraform_iam_group_with_policies" {
    source = "./iam-group-with-policies"
    create_group = "${var.create_group}"
    iam_group_name = "${var.iam_group_name}"
    group_users = "${var.group_users}"
    custom_group_policy_arns = "${var.custom_group_policy_arns}"
    custom_group_policies = "${var.custom_group_policies}"
    attach_iam_self_management_policy = "${var.attach_iam_self_management_policy}"
    iam_self_management_policy_name_prefix = "${var.iam_self_management_policy_name_prefix}"
    aws_account_id = "${var.aws_account_id}"
}

#----deploy networking resource

module "terraform_network" {
    source = "./networking"
    vpc_cidr = "${var.vpc_cidr}"
    pub_sub_cidrs = "${var.pub_sub_cidrs}"
    pri_sub_cidrs = "${var.pri_sub_cidrs}"
    access_ip = "${var.access_ip}"
}


#---------- deploy compute module

module "terraform_compute" {
    source = "./compute"
    key_name = "${var.key_name}"
    key_path = "${var.public_key_path}"
    instance_count = "${var.instance_count}"
    instance_type = "${var.instance_type}"
    pub_cidrs = "${module.terraform_network.pub_cidrs}"
    security_group = "${module.terraform_network.pub_security_group}"
    public_subnet_id = "${module.terraform_network.public_subnet_id}"
}


#-----deploy storage

module "terraform_storage" {
    source = "./storage"
    bucket_name = "${var.bucket_name}"
    log_bucket_name = "${var.log_bucket_name}"
}