#-------variables.tf-----

variable "aws_region" {}

#------ IAM user  variables----

variable "create_user" {}
variable "create_iam_user_login_profile" {}
variable "iam_name" {}
variable "path" {}
variable "force_destroy" {}
variable "pgp_key" {
  type = "list"
}
variable "password_reset_required" {}
variable "password_length" {}
variable "permissions_boundary" {}


#------- iam-group-with-policies variables----

variable "create_group" {}
variable "iam_group_name" {}
variable "group_users" {}
variable "custom_group_policy_arns" {}
variable "custom_group_policies" {
  type = list(map(string))
}
variable "attach_iam_self_management_policy" {}
variable "iam_self_management_policy_name_prefix" {}
variable "aws_account_id" {}

#----- networking variable------

variable "vpc_cidr" {}
variable "pub_sub_cidrs" {
    type = "list"
}
variable "pri_sub_cidrs" {
    type = "list"
}
variable "access_ip" {}


#---------compute variable----------

variable "key_name" {}
variable "public_key_path" {}
variable "instance_count" {
    default = 1
}
variable "instance_type" {}


#---------------storage variables---------

variable "bucket_name" {}
variable "log_bucket_name" {}
