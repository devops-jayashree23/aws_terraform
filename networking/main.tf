#----------networking/main.tf---------------

#---check availabllity zone
data "aws_availability_zones" "available" {
    state = "available"
}


#-----vpc
resource "aws_vpc" "terraform_vpc" {
  cidr_block = "${var.vpc_cidr}"
  instance_tenancy = "default"
  enable_dns_support = "true"
  enable_dns_hostnames = "true"
  
  tags = {
      name = "tf_vpc"
  }
}


#---internet gateway
resource "aws_internet_gateway" "terraform_internet_gateway" {
    vpc_id = "${aws_vpc.terraform_vpc.id}"
    
    tags = {
        name = "tf_igw"
    }
}


#------route tables
resource "aws_route_table" "terraform_public_rt" {
    vpc_id = "${aws_vpc.terraform_vpc.id}"
    
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.terraform_internet_gateway.id}"
    }
    
    tags = {
        name = "tf_public_rt"
    }
}

resource "aws_default_route_table" "terraform_private_rt" {
    default_route_table_id = "${aws_vpc.terraform_vpc.default_route_table_id}"
    
    tags = {
        name = "tf_private_rt"
    }
}


#--------subnets

resource "aws_subnet" "terraform_public_subnet" {
    count = 2
    vpc_id = "${aws_vpc.terraform_vpc.id}"
    cidr_block = "${var.pub_sub_cidrs[count.index]}"
    map_public_ip_on_launch = true
    availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
    
    tags = {
        name = "tf_pub_sub${count.index + 1}"
    }
}

resource "aws_subnet" "terraform_private_subnet" {
    count = 2
    vpc_id = "${aws_vpc.terraform_vpc.id}"
    cidr_block = "${var.pri_sub_cidrs[count.index]}"
    availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
    
    tags = {
        name = "tf_pri_sub${count.index + 1}"
    }
}

#---------------route table association

resource "aws_route_table_association" "public" {
    count = "${length(aws_subnet.terraform_public_subnet)}"
    subnet_id = "${aws_subnet.terraform_public_subnet.*.id[count.index]}"
    route_table_id = "${aws_route_table.terraform_public_rt.id}"
}

resource "aws_route_table_association" "private" {
    count = "${length(aws_subnet.terraform_private_subnet)}"
    subnet_id = "${aws_subnet.terraform_private_subnet.*.id[count.index]}"
    route_table_id = "${aws_default_route_table.terraform_private_rt.id}"
}


#----------------network ACL
resource "aws_network_acl" "terraform_public_nacl" {
    vpc_id = "${aws_vpc.terraform_vpc.id}"
    
    ingress {
        protocol = "tcp"
        rule_no = 100
        action = "allow"
        cidr_block = "${var.access_ip}"
        from_port = 1
        to_port = 65535
    }
    
    egress {
        protocol = "tcp"
        rule_no = 100
        action = "allow"
        cidr_block = "${var.access_ip}"
        from_port = 1
        to_port = 65535
    }
    
    count = "${length(aws_subnet.terraform_public_subnet)}"
    subnet_ids = ["${aws_subnet.terraform_public_subnet.*.id[count.index]}"]
    
    tags = {
        name = "tf_public_nacl"
    }
}


resource "aws_network_acl" "terraform_private_nacl" {
    vpc_id = "${aws_vpc.terraform_vpc.id}"
    
    ingress {
        protocol = "tcp"
        rule_no = 100
        action = "allow"
        cidr_block = "${var.access_ip}"
        from_port = 22
        to_port = 22
    }
    
    egress {
        protocol = "tcp"
        rule_no = "100"
        action = "allow"
        cidr_block = "${var.access_ip}"
        from_port = 1024
        to_port = 65535
    }
    
    count = "${length(aws_subnet.terraform_private_subnet)}"
    subnet_ids = ["${aws_subnet.terraform_private_subnet.*.id[count.index]}"]
    
    tags = {
        name = "tf_private_nacl"
    }
}


#---------------security groups
resource "aws_security_group" "terraform_public_sg" {
    name = "public_sg"
    description = "allow outside traffic to access the web server(EC2 instance.)"
    vpc_id = "${aws_vpc.terraform_vpc.id}"
    
    #SSH
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.access_ip}"]
    }
    
    #--custom tcp
    ingress {
        from_port = 1
        to_port = 65535
        protocol = "tcp"
        cidr_blocks = ["${var.access_ip}"]
    }
    
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["${var.access_ip}"]
    }
}