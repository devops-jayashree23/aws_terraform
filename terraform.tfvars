aws_region = "us-west-2"

#------IAM user----

create_user = 2
create_iam_user_login_profile = true
iam_name = ["paul","lily"]
path = "/"
force_destroy = true
pgp_key = ["keybase:pauliam","keybase:jayashree"]
password_reset_required = true
password_length = 20
permissions_boundary = ""

#------ iam-group-with-policies----

create_group = true
iam_group_name = "dev"
group_users = ["Lily","paul"]
custom_group_policy_arns = []
custom_group_policies = []
attach_iam_self_management_policy = true
iam_self_management_policy_name_prefix = "IAMSelfManagement-"
aws_account_id = ""

#-----networking-----

vpc_cidr = "10.20.0.0/16"
pub_sub_cidrs = [
    "10.20.1.0/24",
    "10.20.2.0/24"
    ]
pri_sub_cidrs = [
    "10.20.3.0/24",
    "10.20.4.0/24"
    ]
access_ip = "0.0.0.0/0"   


#------compute---

key_name = "tf_key"
public_key_path = "/home/ec2-user/.ssh/id_rsa.pub"
instance_count = 2
instance_type = "t2.micro"


#---------storage----

bucket_name = "tfaws-myproject"
log_bucket_name = "tfaws-mylog"