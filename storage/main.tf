#----------storage/main.tf-------

#---create versioning bucket

resource "aws_s3_bucket" "terraform_log" {
  bucket = "${var.log_bucket_name}"
  acl    = "log-delivery-write"

  versioning {
    enabled = true
  }

  lifecycle_rule {
    prefix  = "config/"
    enabled = true

    noncurrent_version_transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      days          = 60
      storage_class = "GLACIER"
    }

    noncurrent_version_expiration {
      days = 90
    }
  }
}
  


# Create kms keys

resource "aws_kms_key" "examplekms" {
  description             = "KMS key 1"
  deletion_window_in_days = 10
}


# create bucket

resource "aws_s3_bucket" "terraform_code" {
  bucket        = "${var.bucket_name}"
  acl           = "private"
  force_destroy = true
  
  
  versioning {
    enabled = true
  }
  
  
  lifecycle_rule {
    id      = "log"
    enabled = true

    prefix = "log/"

    tags = {
      name      = "log"
    }

    transition {
      days          = 30
      storage_class = "STANDARD_IA" # or "ONEZONE_IA"
    }

    transition {
      days          = 60
      storage_class = "GLACIER"
    }

    expiration {
      days = 90
    }
  }
  
  logging {
    target_bucket = "${aws_s3_bucket.terraform_log.id}"
    target_prefix = "log/"
  }
}


# create object


resource "aws_s3_bucket_object" "object" {
  bucket = "${aws_s3_bucket.terraform_code.id}"
  key    = "Dev/pinehead.jpg"
  source = "/home/ec2-user/environment/source/pinehead_1538773172.jpg"
  kms_key_id = "${aws_kms_key.examplekms.arn}"
  
  tags = {
    Name = "tf_bucket"
  }
}

